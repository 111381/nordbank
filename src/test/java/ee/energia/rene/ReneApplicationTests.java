package ee.energia.rene;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.assertj.core.api.Assertions.assertThat;

import ee.energia.rene.repository.AccountDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Runs application functionality as non-independent integration tests with fixed order by method name.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReneApplicationTests {

  @Value("${account.initial.amount}")
  private String accountInitialAmount;

  @Autowired
  private AccountDao accountDao;
  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;
  private BigDecimal transferredAmount = new BigDecimal("0.01");

  @Before
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  public void a_givenWac_whenServletContext_thenItProvidesAccountController() {
    ServletContext servletContext = wac.getServletContext();

    Assert.assertNotNull(servletContext);
    Assert.assertTrue(servletContext instanceof MockServletContext);
    Assert.assertNotNull(wac.getBean("accountController"));
  }

  @Test
  public void b_createTwoAccountsWithDefaultAmountMoney() throws Exception {
    this.mockMvc.perform(post("/account/create"))
        .andExpect(status().isOk())
        .andExpect(content().json("{\"id\":1,\"accountBalance\":" + this.accountInitialAmount + "}"));

    this.mockMvc.perform(post("/account/create"))
        .andExpect(status().isOk())
        .andExpect(content().json("{\"id\":2,\"accountBalance\":" + this.accountInitialAmount + "}"));

    assertThat(this.accountDao.getAccountById(new BigInteger("1")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
    assertThat(this.accountDao.getAccountById(new BigInteger("2")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
  }

  @Test
  public void c_makePaymentWithNegativeAmount_thenExpectFaultMessage_thenExpectDroppingTransaction() throws Exception {
    String paymentAsJson = "{\"relatedAccountId\":\"1\",\"transferredAccountId\":\"2\",\"transferredAmount\":\"-0.01\"}";
    this.mockMvc.perform(post("/account/payment").content(paymentAsJson).contentType("application/json"))
        .andExpect(status().isBadRequest())
        .andExpect(content().json("{\"message\":\"Check transaction values\"}"));
    assertThat(this.accountDao.getAccountById(new BigInteger("1")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
    assertThat(this.accountDao.getAccountById(new BigInteger("2")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
  }

  @Test
  public void d_makePaymentWithTooBigAmount_thenExpectFaultMessage_thenExpectDroppingTransaction() throws Exception {
    String paymentAsJson = "{\"relatedAccountId\":\"1\",\"transferredAccountId\":\"2\",\"transferredAmount\":\"100\"}";
    this.mockMvc.perform(post("/account/payment").content(paymentAsJson).contentType("application/json"))
        .andExpect(status().isBadRequest())
        .andExpect(content().json("{\"message\":\"Not sufficient amount of money on account: " + this.accountInitialAmount + "\"}"));
    assertThat(this.accountDao.getAccountById(new BigInteger("1")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
    assertThat(this.accountDao.getAccountById(new BigInteger("2")).getAccountBalance().toString()).isEqualTo(this.accountInitialAmount);
  }

  @Test
  public void e_makePaymentFromOneAccountToAnother_thenExpectTransactionDone() throws Exception {
    String paymentAsJson = "{\"relatedAccountId\":\"1\",\"transferredAccountId\":\"2\",\"transferredAmount\":\"" + this.transferredAmount.toString() + "\"}";
    this.mockMvc.perform(post("/account/payment").content(paymentAsJson).contentType("application/json"))
        .andExpect(status().isOk())
        .andExpect(content().json("{id:1,relatedAccountId:1,transferredAccountId:2,transferredAmount:" + this.transferredAmount.toString() + "}"));
    assertThat(this.accountDao.getAccountById(new BigInteger("1")).getAccountBalance())
        .isEqualTo(new BigDecimal(this.accountInitialAmount).subtract(this.transferredAmount));
    assertThat(this.accountDao.getAccountById(new BigInteger("2")).getAccountBalance())
        .isEqualTo(new BigDecimal(this.accountInitialAmount).add(this.transferredAmount));
  }

  @Test
  public void f_viewPaymentsMade() throws Exception {
    this.mockMvc.perform(get("/account/transactions?account=1"))
        .andExpect(status().isOk())
        .andExpect(content().json("[{\"id\":1,\"relatedAccountId\":1,\"transferredAccountId\":2,\"transferredAmount\":" + this.transferredAmount.toString() + "}]"));
  }

  @Test
  public void g_makePaymentWithoutBody_thenExpectFault() throws Exception {
    this.mockMvc.perform(post("/account/payment"))
        .andExpect(status().isBadRequest());
  }
}

