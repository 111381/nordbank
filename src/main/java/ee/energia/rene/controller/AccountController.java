package ee.energia.rene.controller;

import ee.energia.rene.model.Error;
import ee.energia.rene.model.Transaction;
import ee.energia.rene.service.AccountService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/account")
public class AccountController {

  private Log log = LogFactory.getLog(AccountController.class);

  @Autowired
  AccountService accountService;

  @PostMapping("/create")
  public ResponseEntity<?> createAccount(HttpServletRequest request) {
    log.info(this.requestToString(request));
    try {
      return new ResponseEntity(accountService.createNewAccountWithDefaultAmountOfMoney(), HttpStatus.OK);
    } catch(DataAccessException e) {
      log.warn("Creating account failed: " + e.getLocalizedMessage());
      return new ResponseEntity(new Error(e.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/payment")
  public ResponseEntity<?> makePayment(@RequestBody Transaction transaction, HttpServletRequest request) {
    log.info(this.requestToString(request));
    try {
      return new ResponseEntity(accountService.makeTransactionBetweenAccounts(transaction), HttpStatus.OK);
    } catch(DataAccessException ie) {
      log.warn(ie.getLocalizedMessage());
      return new ResponseEntity(new Error(ie.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch(IllegalArgumentException e) {
      log.warn(e.getLocalizedMessage());
      return new ResponseEntity(new Error(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping("/transactions")
  public ResponseEntity<?> readTransactionListOfAccount(@RequestParam(value = "account") Long id, HttpServletRequest request) {
    log.info(this.requestToString(request));
    if(id == null) {
      String message = "Request parameter 'account' missing";
      log.warn(message);
      return new ResponseEntity(new Error(message), HttpStatus.BAD_REQUEST);
    }
    try {
      return new ResponseEntity(accountService.getTransactionListForAccount(id), HttpStatus.OK);
    } catch(DataAccessException ie) {
      log.warn(ie.getLocalizedMessage());
      return new ResponseEntity(new Error(ie.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    } catch(IllegalArgumentException e) {
      log.warn(e.getLocalizedMessage());
      return new ResponseEntity(new Error(e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }
  }

  private String requestToString(HttpServletRequest request) {
    return request.getMethod() + " : " + request.getServletPath() + "?" + request.getQueryString();
  }
}
