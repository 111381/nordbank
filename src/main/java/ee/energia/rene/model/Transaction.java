package ee.energia.rene.model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class Transaction {

  private BigInteger id;
  private BigInteger relatedAccountId;
  private BigInteger transferredAccountId;
  private BigDecimal transferredAmount;
}
