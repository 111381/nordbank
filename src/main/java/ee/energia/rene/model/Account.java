package ee.energia.rene.model;

import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Data
public class Account {

  private BigInteger id;
  private BigDecimal accountBalance;
}
