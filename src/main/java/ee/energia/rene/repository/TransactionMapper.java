package ee.energia.rene.repository;

import ee.energia.rene.model.Transaction;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionMapper implements RowMapper<Transaction> {
  @Override
  public Transaction mapRow(ResultSet resultSet, int i) throws SQLException {
    Transaction transaction = new Transaction();
    transaction.setId(BigInteger.valueOf(resultSet.getLong("id")));
    transaction.setRelatedAccountId(BigInteger.valueOf(resultSet.getLong("related_account_id")));
    transaction.setTransferredAccountId(BigInteger.valueOf(resultSet.getLong("transferred_account_id")));
    transaction.setTransferredAmount(resultSet.getBigDecimal("transferred_amount"));
    return null;
  }
}
