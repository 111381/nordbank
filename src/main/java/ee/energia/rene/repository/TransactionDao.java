package ee.energia.rene.repository;

import ee.energia.rene.model.Transaction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Repository
public class TransactionDao {

  private Log log = LogFactory.getLog(TransactionDao.class);

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public List<Transaction> getTransactionsByAccountId(BigInteger accountId) {
    String sql = "SELECT * FROM transaction WHERE related_account_id = ?";
    List<Transaction> transactionList = this.jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Transaction.class), accountId);
    return transactionList;
  }

  public BigDecimal getAccountBalance(BigInteger accountId) {
    BigDecimal balance;
    String sqlBalance = "SELECT account_balance FROM account WHERE id = ?;";
    balance = this.jdbcTemplate.queryForObject(sqlBalance, BigDecimal.class, accountId);
    return balance;
  }

  public void updateAccountBalance(BigInteger accountId, BigDecimal accountBalance) {
    String sqlUpdate = "UPDATE account SET account_balance = ? WHERE id = ?;";
    this.jdbcTemplate.update(sqlUpdate, accountBalance, accountId);
  }

  public void createTransaction(Transaction transaction) {
    String sqlCreateTransaction = "INSERT INTO transaction (related_account_id, transferred_account_id, transferred_amount) values (?, ?, ?);";
    this.jdbcTemplate.update(sqlCreateTransaction, transaction.getRelatedAccountId(), transaction.getTransferredAccountId(), transaction.getTransferredAmount());
    String sqlId = "SELECT IDENTITY();";
    transaction.setId(this.jdbcTemplate.queryForObject(sqlId, BigInteger.class));
  }
}
