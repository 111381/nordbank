package ee.energia.rene.repository;

import ee.energia.rene.model.Account;

import org.springframework.jdbc.core.RowMapper;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMapper implements RowMapper<Account> {

  @Override
  public Account mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    Account acc = new Account();
    acc.setId(BigInteger.valueOf(resultSet.getLong("id")));
    acc.setAccountBalance(resultSet.getBigDecimal("account_balance"));
    return acc;
  }
}
