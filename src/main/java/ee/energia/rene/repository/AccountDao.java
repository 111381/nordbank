package ee.energia.rene.repository;

import ee.energia.rene.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.math.BigInteger;

@Repository
public class AccountDao {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  public Account createAccount(BigDecimal initialAmount) {
    String sqlInsert = "INSERT INTO account (account_balance) VALUES (?);";
    String sqlId = "SELECT IDENTITY();";
    this.jdbcTemplate.update(sqlInsert, initialAmount);
    BigInteger accountId = this.jdbcTemplate.queryForObject(sqlId, BigInteger.class);
    return this.getAccountById(accountId);
  }

  public Account getAccountById(BigInteger id) {
    String sql = "SELECT * FROM account WHERE id = ?";
    return this.jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Account.class), id);
  }
}
