package ee.energia.rene;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReneApplication.class, args);
	}
}