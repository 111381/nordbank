package ee.energia.rene.service;

import ee.energia.rene.model.Account;
import ee.energia.rene.model.Transaction;
import ee.energia.rene.repository.AccountDao;
import ee.energia.rene.repository.TransactionDao;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Service
public class AccountService {

  private Log log = LogFactory.getLog(AccountService.class);

  private static final int ZERO = 0;

  @Value("${account.initial.amount}")
  private String accountInitialAmount;

  @Autowired
  private AccountDao accountDao;
  @Autowired
  private TransactionDao transactionDao;

  @Transactional
  public Account createNewAccountWithDefaultAmountOfMoney() {
    BigDecimal initialAmount = new BigDecimal(this.accountInitialAmount);
    Account account;
    account = accountDao.createAccount(initialAmount);
    log.info("Created account: " + account.toString());
    return account;
  }

  @Transactional
  public Transaction makeTransactionBetweenAccounts(Transaction transaction) {
    if(this.areTransactionValuesBiggerThanZero(transaction) && this.areAccountsDifferent(transaction)) {
      BigDecimal balanceFrom;
      BigDecimal balanceTo;
      try {
        balanceFrom = transactionDao.getAccountBalance(transaction.getRelatedAccountId());
      } catch(EmptyResultDataAccessException e) {
        String message = "No account found with id: " + transaction.getRelatedAccountId().toString();
        throw new IllegalArgumentException(message);
      }
      log.info("Initial amount from account: " + balanceFrom.toString());
      if(balanceFrom.compareTo(transaction.getTransferredAmount()) < ZERO) {
        String message = "Not sufficient amount of money on account: " + balanceFrom.toString();
        throw new IllegalArgumentException(message);
      }
      try {
        balanceTo = transactionDao.getAccountBalance(transaction.getTransferredAccountId());
      } catch(EmptyResultDataAccessException e) {
        String message = "No account found with id: " + transaction.getTransferredAccountId().toString();
        throw new IllegalArgumentException(message);
      }
      log.info("Initial amount transfering>To account: " + balanceTo.toString());
      transactionDao.updateAccountBalance(transaction.getRelatedAccountId(), balanceFrom.subtract(transaction.getTransferredAmount()));
      transactionDao.updateAccountBalance(transaction.getTransferredAccountId(), balanceTo.add(transaction.getTransferredAmount()));
      transactionDao.createTransaction(transaction);
      log.info("Transaction: " + transaction.toString() + " committed.");
    } else {
      throw new IllegalArgumentException("Check transaction values");
    }
    return transaction;
  }

  public List<Transaction> getTransactionListForAccount(long accountId) {
    if(accountId > ZERO) {
      return this.transactionDao.getTransactionsByAccountId(BigInteger.valueOf(accountId));
    } else {
      throw new IllegalArgumentException("Account ID must be positive number");
    }
  }

  private boolean areTransactionValuesBiggerThanZero(Transaction transaction) {
    String messageNegative = "Transaction values can't be zero or negative: " + transaction.toString();
    String messageMissing = "Transaction values can't be missing: " + transaction.toString();
    try {
      if(transaction.getTransferredAmount().compareTo(BigDecimal.ZERO) <= ZERO
          || transaction.getRelatedAccountId().compareTo(BigInteger.ZERO) <= ZERO
          || transaction.getTransferredAccountId().compareTo(BigInteger.ZERO) <= ZERO) {
        log.warn(messageNegative);
        return false;
      }
      return true;
    } catch(NullPointerException npe) {
      throw new IllegalArgumentException(messageMissing);
    }
  }

  private boolean areAccountsDifferent(Transaction transaction) {
    String messageDifferent = "Transaction accounts should be different: " + transaction.toString();
    String messageMissing = "Transaction values can't be missing: " + transaction.toString();
    try {
      if(transaction.getRelatedAccountId().compareTo(transaction.getTransferredAccountId()) == ZERO) {
        log.warn(messageDifferent);
        return false;
      }
      return true;
    } catch(NullPointerException npe) {
      throw new IllegalArgumentException(messageMissing);
    }
  }
}
