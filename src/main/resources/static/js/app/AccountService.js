'use strict'

angular.module('bank.services', []).factory('AccountService',
    ["$http", "CONSTANTS", function ($http, CONSTANTS) {
        var service = {};

        service.createAccount = function () {
            var url = CONSTANTS.createAccount;
            return $http.post(url);
        }

        service.accountPayment = function (transaction) {
            var url = CONSTANTS.accountPayment;
            return $http.post(url, transaction);
        }

        service.accountTransactions = function (accountId) {
            var url = CONSTANTS.accountTransactions;
            return $http.get(url, {
                params: {
                    account: accountId
                }
            });
        }

        return service;
    }]);