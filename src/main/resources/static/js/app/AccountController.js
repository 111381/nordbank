'use strict'

var module = angular.module('bank.controllers', []);
module.controller("AccountController", ["$scope", "AccountService",
    function ($scope, AccountService) {

        $scope.transaction = {
            relatedAccountId: null,
            transferredAccountId: null,
            transferredAmount: null
        };
        $scope.accountId = null;
        $scope.transactionList = [];

        $scope.createAccount = function () {
            AccountService.createAccount().then(function (value) {
                console.log("Create account");
                $scope.account = value.data;
            }, function (reason) {
                $scope.account = reason.data;
            });
        }

        $scope.accountPayment = function () {
            AccountService.accountPayment($scope.transaction).then(function (value) {
                console.log("Make payment");
                $scope.transactionMade = value.data;
            }, function (reason) {
                $scope.transactionMade = reason.data;
            });
        }

        $scope.accountTransactions = function () {
            AccountService.accountTransactions($scope.accountId).then(function (value) {
                console.log("Get transactions");
                $scope.transactionList = value.data;
                if($scope.transactionList.length == 0) {
                    var message = "No transactions made from account " + $scope.accountId;
                    $scope.transactionList.push(message);
                }
            }, function (reason) {
                $scope.transactionList.push(reason.data);
            });
        }
    }]);