'use strict'

var bankApp = angular.module('bank', ['ui.bootstrap', 'bank.controllers', 'bank.services']);
bankApp.constant("CONSTANTS", {
    createAccount: "/account/create",
    accountPayment: "/account/payment",
    accountTransactions: "/account/transactions"
});