CREATE TABLE account (
  id IDENTITY PRIMARY KEY NOT NULL,
  account_balance DECIMAL(17,2) NOT NULL
);

CREATE TABLE transaction (
  id IDENTITY PRIMARY KEY NOT NULL,
  related_account_id BIGINT NOT NULL,
  transferred_account_id BIGINT NOT NULL,
  transferred_amount DECIMAL(17,2) NOT NULL
);