ASSIGNMENT:

Write a simple back-end REST service for a banking system. The application only needs to support making payments between two accounts within the system.
The application should use an SQL database that can be a pure Java-based db like HSQLDB or H2 which is simple to get up and running (from your code) or a PostgreSQL db.
No authentication is needed for the REST service. Use standard HTTP methods to request REST endpoints and expect responses as JSON.
Use Clean Code Principles.

Functionality (REST endpoints):

• Create a new account (with a default amount of money)
• Make a payment from one account to another
• View the payments made from an account

Technology:

• Java 8
• Use Maven for building your application
• Use a self-contained web-server (e.g. embedded Jetty, Tomcat, Undertow)
• Write automated tests

SOLUTION:

Implemented just and only the functionality requested.
All transaction and control logic is built in the backend.
Very basic AngularJS UI is attached. No input controls implemented, only raw presentation logic. If input values are not correct, shows JSON message from 400 response.

1. Open command prompt/shell and run: git clone https://111381@bitbucket.org/111381/nordbank.git
2. Change to cloned directory.
3. Build project using Maven: mvn clean package
4. Run project: mvn spring-boot:run
5. Application server root: http://localhost:8000/
6. Press Ctrl-C to stop the server or send shutdown request to SpringBoot Actuator (with curl by example): curl -i -X POST http://localhost:8000/actuator/shutdown

The application can be used also without UI.

1. Create a new account (with a default amount of money): curl -X POST http://localhost:8000/account/create
It creates new client account with unic 'id' and default amount of money (from conf 'account.initial.amount')
Request returns created account.

2. Make a payment from one account to another: curl -i -X POST -H "Content-Type: application/json" -d "{\"id\":\"0\",\"relatedAccountId\":\"1\",\"transferredAccountId\":\"2\",\"transferredAmount\":\"0.01\"}" http://localhost:8000/account/payment
Request returns payment that is made.

3. View the payments made from an account: curl http://localhost:8000/account/transactions?account=1
Request returns list of payments made from given account ID.